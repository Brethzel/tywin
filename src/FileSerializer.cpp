#include "../include/FileSerializer.h"

#include <QJsonDocument>
#include <QFile>
#include <QDir>
#include <QDirIterator>
#include <QVariant>

#include <QDebug>

namespace Tywin {

FileSerializer::FileSerializer(
    QObject *parent,
    QString folder,
    QString extension
):
    Serializer(parent),
    m_folder(folder),
    m_extension(extension)
{

}

void FileSerializer::lookup() {
    QDirIterator it(
        m_folder,
        QStringList() << "*.json",
        QDir::Files,
        QDirIterator::Subdirectories
    );

    while (it.hasNext()) {
        it.next(); // not last?

        QFile file;
        file.setFileName(it.filePath());
        file.open(QIODevice::ReadOnly);
        QByteArray array(file.readAll());
        file.close();

        QJsonDocument document(QJsonDocument::fromJson(array));
        emit jsonFound(document.object());
    }
}

void FileSerializer::serialize(QJsonObject *object) {
    QFile file;
    QJsonDocument document(*object);
    QString currentFolder(QDir::currentPath());

    QDir::setCurrent(m_folder);
    QString id(object->value("objectId").toString());

    if (id.isEmpty())
        return; // todo: throw an error?

    file.setFileName(id + m_extension);
    file.open(QIODevice::WriteOnly);
    QByteArray array(document.toJson());
    file.write(array, array.size());
    file.close();

    QDir::setCurrent(currentFolder);
}

}
