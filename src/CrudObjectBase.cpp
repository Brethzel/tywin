//#include "../include/CrudObjectBase.h"

//#include "../include/JsonConverter.h"

//#include <QJsonObject>

//namespace Tywin {

//CrudObjectBase::CrudObjectBase(Serializer *serializer)
//:
//    QObject(serializer),
//    m_serializer(serializer),
//    m_metaUpdateSlot(nullptr),
//    m_autoUpdate(false)
//{}

//void CrudObjectBase::createNow() {
//    createNow(this);
//}

//void CrudObjectBase::createNow(QObject *object) {
//    if (object == nullptr)
//        return;

//    QJsonObject jsonObject;
//    QJsonObject serialized(JsonConverter::toJson(object));

//    jsonObject.insert("message", "create");
//    jsonObject.insert("objectId", serialized.value("id"));
//    jsonObject.insert("object", serialized);

//    m_serializer->serialize(&jsonObject);

//    if (m_autoUpdate)
//        connect();
//}

//void CrudObjectBase::updateNow() {
//    QJsonObject object;
//    QJsonObject serialized(JsonConverter::toJson(this));

//    object.insert("message", "update");
//    object.insert("objectId", serialized.value("id"));
//    object.insert("object", serialized);

//    m_serializer->serialize(&object);
//}

//void CrudObjectBase::deleteNow() {
//    QJsonObject object;
//    QJsonObject serialized(JsonConverter::toJson(this));

//    object.insert("message", "delete");
//    object.insert("objectId", serialized.value("id"));
//    object.insert("object", serialized);

//    m_serializer->serialize(&object);
//}

//void CrudObjectBase::autoUpdate() {
//    m_autoUpdate = true;
//}

//void CrudObjectBase::connect() {
//    const QMetaObject* metaObject(this->metaObject());

//    if (m_metaUpdateSlot == nullptr) {
//        const int count(metaObject->methodCount());
//        for (int i(0); i < count; i++) {
//            QMetaMethod metaMethod(metaObject->method(i));

//            if (metaMethod.name() == "updateNow") {
//                m_metaUpdateSlot = &metaMethod;
//                break;
//            }
//        }
//    }

//    const int count(metaObject->propertyCount());
//    for (int i(0); i < count; i++) {
//        QMetaProperty metaProperty(metaObject->property(i));

//        if (!metaProperty.isStored(this) || !metaProperty.hasNotifySignal())
//            continue;

//        QString name(metaProperty.name());
//        if (name == "objectName")
//            continue;

//        QMetaMethod metaSignal(metaProperty.notifySignal());
//        QObject::connect(this, metaSignal, this, *m_metaUpdateSlot);
//    }
//}

//}
