#include "../include/JsonConverter.h"
#include <QJsonDocument>
#include <QMetaProperty>
#include <QColor>

#include <QDebug>
#include <QRegularExpression>

namespace Tywin {

QJsonObject JsonConverter::toJson(const QObject *object, JsonConverter::Conversions conversion) {
    QJsonObject retVal;
    if (object == nullptr)
        return retVal;

    const QMetaObject* metaObject(object->metaObject());

    retVal.insert("className", metaObject->className());

    const int count(metaObject->propertyCount());
    for (int i(0); i < count; i++) {
        QMetaProperty metaProperty(metaObject->property(i));

        if (metaProperty.isStored(object) == false)
            continue;

        QString name(metaProperty.name());
        if (name == "objectName")
            continue;

        if (conversion == Conversions::CAMELCASE_TO_UNDERSCORE)
            name = fromCamelCase(name);

        // todo: add lists and objects properties
        QVariant value(metaProperty.read(object));
//        qDebug() << "property:" << name << "|" << value.toString();
        retVal.insert(name, QJsonValue::fromVariant(value));
    }

    return retVal;
}

void JsonConverter::fromJson(QObject *object, const QJsonObject &json, JsonConverter::Conversions conversion) {
    if (object == nullptr)
        return;

    const QMetaObject* metaObject(object->metaObject());

    int count(metaObject->propertyCount());
    for (int i(0); i < count; i++) {
        QMetaProperty metaProperty(metaObject->property(i));

        // not sure why it was there, but it's obviously blocking...
        // todo: double check
//        if (metaProperty.isStored(object))
//            continue;

        QString name(metaProperty.name());
        if (name == "objectName")
            continue;

        if (conversion == Conversions::UNDERSCORE_TO_CAMEL_CASE)
            name = fromCamelCase(name);

        qDebug() << name << metaProperty.isWritable() << json.value(name).toVariant();
        // todo: add lists and objects properties
        metaProperty.write(object, json.value(name).toVariant());
    }
}

QString JsonConverter::fromCamelCase(const QString &s) {
    static QRegularExpression regExp1 {"(.)([A-Z][a-z]+)"};
    static QRegularExpression regExp2 {"([a-z0-9])([A-Z])"};

    QString result = s;
    result.replace(regExp1, "\\1_\\2");
    result.replace(regExp2, "\\1_\\2");

    return result.toLower();
}

QString JsonConverter::toCamelCase(const QString &s) {
    QStringList parts = s.split('_', QString::SkipEmptyParts);
    for (int i=1; i<parts.size(); ++i)
        parts[i].replace(0, 1, parts[i][0].toUpper());

    return parts.join("");
}

}
