#include "../include/CrudObject.h"
#include "../include/JsonConverter.h"

#include <QJsonObject>

#include <QDebug>

namespace Tywin {

CrudObject::CrudObject(Serializer *serializer)
:
    QObject(serializer),
    m_serializer(serializer),
    m_metaUpdateSlot(nullptr),
    m_autoUpdate(false)
{}

void CrudObject::createNow() {
    QJsonObject jsonObject;
    QJsonObject serialized(JsonConverter::toJson(this));

    jsonObject.insert("message", "create");
    jsonObject.insert("objectId", serialized.value("id"));
    jsonObject.insert("object", serialized);

    m_serializer->serialize(&jsonObject);

    if (m_autoUpdate)
        connect();
}

void CrudObject::updateNow() {
    QJsonObject object;
    QJsonObject serialized(JsonConverter::toJson(this));

    object.insert("message", "update");
    object.insert("objectId", serialized.value("id"));
    object.insert("object", serialized);

    m_serializer->serialize(&object);
}

void CrudObject::deleteNow() {
    QJsonObject object;
    QJsonObject serialized(JsonConverter::toJson(this));

    object.insert("message", "delete");
    object.insert("objectId", serialized.value("id"));
    object.insert("object", serialized);

    m_serializer->serialize(&object);
}

void CrudObject::autoUpdate() {
    m_autoUpdate = true;
}

void CrudObject::connect() {
    const QMetaObject* metaObject(this->metaObject());

    if (m_metaUpdateSlot == nullptr) {
        const int count(metaObject->methodCount());
        for (int i(0); i < count; i++) {
            QMetaMethod metaMethod(metaObject->method(i));

            if (metaMethod.name() == "updateNow") {
                m_metaUpdateSlot = &metaMethod;
                break;
            }
        }
    }

    const int count(metaObject->propertyCount());
    for (int i(0); i < count; i++) {
        QMetaProperty metaProperty(metaObject->property(i));

        if (!metaProperty.isStored(this) || !metaProperty.hasNotifySignal())
            continue;

        QString name(metaProperty.name());
        if (name == "objectName")
            continue;

        QMetaMethod metaSignal(metaProperty.notifySignal());
        QString signalName(metaSignal.methodSignature());

        // This is a quite ugly hack but sadly I haven't found a better way to
        // achieve this.
        // TODO: adding a #if WITH_VARYS
        if (signalName.contains("valueChanged_QML")) {
            signalName.replace("valueChanged_QML", "mutableValueChanged_QML");

            int index(metaObject->indexOfSignal(
                QMetaObject::normalizedSignature(signalName.toLatin1()))
            );

            if (index >= 0)
                metaSignal = metaObject->method(index);
        }

        QObject::connect(this, metaSignal, this, *m_metaUpdateSlot);
    }
}

}
