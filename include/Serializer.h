#ifndef TYWIN_SERIALIZER_H
#define TYWIN_SERIALIZER_H

#include <QObject>
#include <QJsonObject>

namespace Tywin {

class Serializer : public QObject {
    Q_OBJECT

public:
    using QObject::QObject;

    virtual void serialize(QJsonObject* object) = 0;
};

}

#endif
