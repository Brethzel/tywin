//#ifndef TYWIN_CRUDOBJECTBASE_H
//#define TYWIN_CRUDOBJECTBASE_H

//#include <QObject>
//#include "Serializer.h"
//#include <QMetaProperty>

//namespace Tywin {

//class CrudObjectBase : private QObject {
//    Q_OBJECT

//public:
//    explicit CrudObjectBase(Serializer* serializer);

//public slots:
//    void createNow();
//    void createNow(QObject* object);

//    // be careful that this method is used with reflection in connect(),
//    // make sure you update connect() as well if you change the name of this
//    // method.
//    void updateNow();
//    void deleteNow();

//    // todo: add createOn(...), updateOn(...), deleteOn(...)?

//    // todo: add noUpdate(), noDelete()?

//    // todo: add freeze(), unfreeze()?

//protected:
//    // could be parent but then we need to cast all the time
//    Serializer* m_serializer;
//    QMetaMethod* m_metaUpdateSlot;
//    bool m_autoUpdate;

//    void autoUpdate();
//    void connect();
//};

//}

//#endif
