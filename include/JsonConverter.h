#ifndef TYWIN_JSONCONVERTER_H
#define TYWIN_JSONCONVERTER_H

#include <QObject>
#include <QJsonObject>
#include <QMetaProperty>

#include "CrudObject.h"

namespace Tywin {

class JsonConverter : public QObject {
    Q_OBJECT

public:
    using QObject::QObject;

    enum Conversions {
        NONE,
        UNDERSCORE_TO_CAMEL_CASE,
        CAMELCASE_TO_UNDERSCORE
    };

    static QJsonObject toJson(
        const QObject* object,
        Conversions conversion = Conversions::NONE
    );

    static void fromJson(
        QObject* object,
        const QJsonObject& json,
        Conversions conversion = Conversions::NONE
    );

    static QString fromCamelCase(const QString& s);
    static QString toCamelCase(const QString& s);
};

}

#endif
