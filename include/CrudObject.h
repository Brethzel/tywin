#ifndef TYWIN_CRUDOBJECT_H
#define TYWIN_CRUDOBJECT_H

#include <QObject>
#include "Serializer.h"
#include <QMetaProperty>

namespace Tywin {

// solutions:
//      * createNow(QObject*), CrudObject isn't a QObject anymore and reimplementing all the time
//      * CrudObject is a BindingNotifier (could be with a compilation flag)
//      * setObject(QObject*) called in each constructor, CrudObject isn't a QObject anymore

class CrudObject : public QObject {
    Q_OBJECT

public:
    explicit CrudObject(Serializer* serializer);

public slots:
    void createNow();

    // be careful that this method is used with reflection in connect(),
    // make sure you update connect() as well if you change the name of this
    // method.
    void updateNow();
    void deleteNow();

    // todo: add createOn(...), updateOn(...), deleteOn(...)?

    // todo: add noUpdate(), noDelete()?

    // todo: add freeze(), unfreeze()?

protected:
    // could be parent but then we need to cast all the time
    Serializer* m_serializer;
    QMetaMethod* m_metaUpdateSlot;
    bool m_autoUpdate;

    void autoUpdate();
    void connect();
};

}

#endif
