#ifndef TYWIN_SERIALIZABLEOBJECT_H
#define TYWIN_SERIALIZABLEOBJECT_H

#include <QObject>
#include <QMetaProperty>

namespace Tywin {

class SerializableObject : public QObject {
    Q_OBJECT

public:
    explicit SerializableObject(
        QObject* parent = nullptr,
        bool autoConnect = true
    );

    // this auto connects all the properties to the signal "objectChanged"
    void autoConnect();

signals:
    void objectChanged();

private:
    QMetaMethod* m_metaUpdateSlot;
};

}
#endif
