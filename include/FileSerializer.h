#ifndef TYWIN_FILESERIALIZER_H
#define TYWIN_FILESERIALIZER_H

#include <QObject>

#include "Serializer.h"

namespace Tywin {

class FileSerializer : public Serializer {
    Q_OBJECT

public:
    explicit FileSerializer(
        QObject* parent,
        QString folder,
        QString extension = ".json"
    );

    void lookup();

    // Serializer interface
public:
    void serialize(QJsonObject *object);

signals:
    void jsonFound(QJsonObject object);

protected:
    QString m_folder;
    QString m_extension;
};

}

#endif
