# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <gilles.fernandez10@gmail.com> wrote this file. As long as you retain this
# notice you can do whatever you want with this stuff. If we meet some day,
# and you think this stuff is worth it, you can buy me a (belgian) beer in
# return.
# Gilles Fernandez
# ----------------------------------------------------------------------------

macro(generate_qrc_file FILENAME FILES)
    file(WRITE ${FILENAME} "<RCC>\n")
    file(APPEND ${FILENAME} "    <qresource prefix=\"/\">\n")

    foreach(FILE ${FILES})
        file(APPEND ${FILENAME} "        <file>${FILE}</file>\n")
    endforeach(FILE)

    file(APPEND ${FILENAME} "    </qresource>\n")
    file(APPEND ${FILENAME} "</RCC>")

    list(APPEND RES_FILES ${FILENAME})
endmacro()

macro(add_qt_dependency DEPENDENCY)
    list(APPEND QT_DEPENDENCIES ${DEPENDENCY})
endmacro()

macro(add_include_path INCLUDE_PATH)
    list(APPEND INCLUDE_PATHS ${INCLUDE_PATH})
endmacro()

macro(generate_pro_file FILENAME TEMPLATE QT_DEPENDENCIES HEADER_FILES SOURCE_FILES OBJECTIVE_SOURCE_FILES RESOURCE_FILES LIBRARIES INCLUDE_PATHS WITH_FRAMELESS_WINDOW)
    file(WRITE ${FILENAME} "TEMPLATE = ${TEMPLATE}\n\n")

    file(APPEND ${FILENAME} "QT +=")
    foreach (QT_DEPENDENCY ${QT_DEPENDENCIES})
        file(APPEND ${FILENAME} " \\\n")

        if (${QT_DEPENDENCY} STREQUAL "test")
            file(APPEND ${FILENAME} "      testlib")
        else()
            string(TOLOWER ${QT_DEPENDENCY} LOWER_QT_DEPENDENCY)
            file(APPEND ${FILENAME} "      ${LOWER_QT_DEPENDENCY}")
        endif()
    endforeach(QT_DEPENDENCY)

    file(APPEND ${FILENAME} "\n\n")

    file(APPEND ${FILENAME} "CONFIG += c++14\n\n")

    file(APPEND ${FILENAME} "HEADERS +=")
    foreach (HEADER_FILE ${HEADER_FILES})
        file(APPEND ${FILENAME} " \\\n")
        file(APPEND ${FILENAME} "           ${HEADER_FILE}")
    endforeach(HEADER_FILE)

    file(APPEND ${FILENAME} "\n\n")

    file(APPEND ${FILENAME} "SOURCES +=")
    foreach (SOURCE_FILE ${SOURCE_FILES})
        file(APPEND ${FILENAME} " \\\n")
        file(APPEND ${FILENAME} "           ${SOURCE_FILE}")
    endforeach(SOURCE_FILE)

    file(APPEND ${FILENAME} "\n\n")

    file(APPEND ${FILENAME} "RESOURCES +=")
    foreach (RES_FILE ${RESOURCE_FILES})
        file(APPEND ${FILENAME} " \\\n")
        file(APPEND ${FILENAME} "             ${RES_FILE}")
    endforeach(RES_FILE)

    file(APPEND ${FILENAME} "\n\n")

    file(APPEND ${FILENAME} "INCLUDEPATH += ${CMAKE_CURRENT_BINARY_DIR}")
    foreach (LIBRARY ${LIBRARIES})
        file(APPEND ${FILENAME} " \\\n")
        file(APPEND ${FILENAME} "               ${${LIBRARY}_INCLUDE_DIRS}")
    endforeach(LIBRARY)

    foreach (INCLUDE_PATH ${INCLUDE_PATHS})
        file(APPEND ${FILENAME} " \\\n")
        file(APPEND ${FILENAME} "               ${INCLUDE_PATH}")
    endforeach(INCLUDE_PATH)

    file(APPEND ${FILENAME} "\n\n")

    file(APPEND ${FILENAME} "LIBS +=")
    foreach (LIBRARY ${LIBRARIES})
        file(APPEND ${FILENAME} " \\\n")
        file(APPEND ${FILENAME} "        -L${${LIBRARY}_LIBRARY_PATH} -l${LIBRARY}")
    endforeach(LIBRARY)

    if (${WITH_FRAMELESS_WINDOW})
        file(APPEND ${FILENAME} "\n\n")
        file(APPEND ${FILENAME} "win32 {\n")
        file(APPEND ${FILENAME} "    SOURCES += \\\n")
        file(APPEND ${FILENAME} "               src/internal/FramelessWindow/QWinWidget.cpp \\\n")
        file(APPEND ${FILENAME} "               src/internal/FramelessWindow/WinNativeWindow.cpp \n")
        file(APPEND ${FILENAME} "\n")
        file(APPEND ${FILENAME} "    HEADERS += \\\n")
        file(APPEND ${FILENAME} "               src/internal/FramelessWindow/QWinWidget.h \\\n")
        file(APPEND ${FILENAME} "               src/internal/FramelessWindow/WinnativeWindow.h \n")
        file(APPEND ${FILENAME} "\n")
        file(APPEND ${FILENAME} "    LIBS += \\\n")
        file(APPEND ${FILENAME} "            -L\"C:\\Program Files\\Microsoft SDKs\\Windows\\v7.1\\Lib\" \\\n")
        file(APPEND ${FILENAME} "            -ldwmapi \\\n")
        file(APPEND ${FILENAME} "            -lgdi32 \n")
        file(APPEND ${FILENAME} "}\n\n")

        file(APPEND ${FILENAME} "mac {\n")
        file(APPEND ${FILENAME} "    OBJECTIVE_SOURCES += \\\n")

        foreach (OBJECTIVE_SOURCE ${OBJECTIVE_SOURCE_FILES})
            file(APPEND ${FILENAME} "                         ${OBJECTIVE_SOURCE} \n")
        endforeach()
#        file(APPEND ${FILENAME} "                         src/internal/FramelessWindow/OSXHideTitleBar.mm \n")
        file(APPEND ${FILENAME} "    HEADERS += \\\n")
        file(APPEND ${FILENAME} "               src/internal/FramelessWindow/OSXHideTitleBar.h \n")
        file(APPEND ${FILENAME} "\n")
        file(APPEND ${FILENAME} "    LIBS += \\\n")
        file(APPEND ${FILENAME} "            -framework Foundation # -framework Cocoa # no clue why it's sometimes needed and sometimes not... would it be related to mac os vs ios? Or ios versions?\n")
        file(APPEND ${FILENAME} "\n")
        file(APPEND ${FILENAME} "    INCLUDEPATH += \\\n")
        file(APPEND ${FILENAME} "                   /System/Library/Frameworks/Foundation.framework/Versions/C/Headers \n")
        file(APPEND ${FILENAME} "}\n")
    endif()

endmacro()

macro(add_qt_library NAME TYPE QT_DEPENDENCIES HEADER_FILES SOURCE_FILES QML_FILES RESOURCE_FILES OTHER_FILES)
    include_directories(${CMAKE_CURRENT_BINARY_DIR})

    foreach (QT_DEPENDENCY ${QT_DEPENDENCIES})
        find_package(Qt5 COMPONENTS ${QT_DEPENDENCY})
    endforeach()

    add_library(
            ${NAME}
            ${TYPE}
            ${HEADER_FILES}
            ${SOURCE_FILES}
            ${QML_FILES}
            ${RESOURCE_FILES}
            ${OTHER_FILES}
    )

    # Setup compiler directives
    set_property(TARGET ${NAME} PROPERTY CXX_STANDARD 14)
    set_property(TARGET ${NAME} PROPERTY CXX_STANDARD_REQUIRED ON)

    foreach (QT_DEPENDENCY ${QT_DEPENDENCIES})
        target_link_libraries(${NAME} Qt5::${QT_DEPENDENCY})
    endforeach()

    foreach (HEADER_FILE ${HEADER_FILES})
    	get_filename_component(HEADER_FILE_NAME ${HEADER_FILE} NAME_WE)
    	list(APPEND MOC_FILES "moc_${HEADER_FILE_NAME}.cpp")
    endforeach()

    list(APPEND MOC_FILES "${PROJECT_NAME}_automoc.cpp")

    set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES ${MOC_FILES})
endmacro()

macro(add_qt_executable NAME QT_DEPDENDENCIES HEADER_FILES SOURCE_FILES OBJECTIVE_SOURCE_FILES QML_FILES RESOURCE_FILES LIBRARIES OTHER_FILES, GENERATE_BUNDLE)
    include_directories(${CMAKE_CURRENT_BINARY_DIR})
    set(CMAKE_AUTOMOC ON)
    set(CMAKE_AUTORCC ON)

    foreach (QT_DEPENDENCY ${QT_DEPENDENCIES})
        find_package(Qt5 COMPONENTS ${QT_DEPENDENCY})
    endforeach()

    # WIN32: add resource file
    if (WIN32)
        list(APPEND SRC_FILES src/win32/demo.rc)
    endif()

    # OS X: generate bundle
    if (APPLE AND ${GENERATE_BUNDLE})
        set(MACOSX_BUNDLE_BUNDLE_NAME ${PROJECT_NAME})
        set(MACOSX_BUNDLE_BUNDLE_VERSION ${${NAME}_VERSION})
        set(MACOSX_BUNDLE_COPYRIGHT ${${NAME}_COPYRIGHT})
        set(MACOSX_BUNDLE_GUI_IDENTIFIER "com.gfz.${NAME}")
        set(MACOSX_BUNDLE_ICON_FILE ${${NAME}_ICNS_FILENAME_WE})

        list(APPEND RES_FILES ${${NAME}_ICNS_PATH})
        set_source_files_properties(${${NAME}_ICNS_PATH} PROPERTIES MACOSX_PACKAGE_LOCATION "Resources")
        set(MACOSX_PACKAGE_LOCATION "Resources")
    endif()

    set(MACOSX_BUNDLE "")
    if (GENERATE_BUNDLE)
        set(MACOSX_BUNDLE "MACOSX_BUNDLE")
    endif()

    add_executable(
        ${NAME}
        WIN32
        ${MACOSX_BUNDLE}
        ${HEADER_FILES}
        ${SOURCE_FILES}
        ${OBJECTIVE_SOURCE_FILES}
        ${QML_FILES}
        ${RESOURCE_FILES}
        ${OTHER_FILES}
    )

    # Setup compiler directives
    set_property(TARGET ${NAME} PROPERTY CXX_STANDARD 14)
    set_property(TARGET ${NAME} PROPERTY CXX_STANDARD_REQUIRED ON)

    foreach (QT_DEPENDENCY ${QT_DEPENDENCIES})
        target_link_libraries(${NAME} Qt5::${QT_DEPENDENCY})
    endforeach()

    foreach (LIBRARY ${LIBRARIES})
        include_directories(${${LIBRARY}_INCLUDE_DIRS})
        target_link_libraries(${NAME} ${${LIBRARY}_LIBRARIES})
    endforeach()

    foreach (HEADER_FILE ${HEADER_FILES})
        get_filename_component(HEADER_FILE_NAME ${HEADER_FILE} NAME_WE)
        list(APPEND MOC_FILES "moc_${HEADER_FILE_NAME}.cpp")
    endforeach()

    list(APPEND MOC_FILES "${PROJECT_NAME}_automoc.cpp")

    set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES ${MOC_FILES})
endmacro()

macro(add_qmake_build NAME LIBRARIES)
    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/qmake-build)

    set(QMAKEARGS "")
    if (CMAKE_BUILD_TYPE STREQUAL "Debug")
        string(CONCAT QMAKEARGS ${QMAKEARGS} "CONFIG+=debug CONFIG+=qml_debug")
    else ()
        string(CONCAT QMAKEARGS ${QMAKEARGS} "CONFIG+=release")
    endif()

    if (APPLE)
        set(EXPORTARGS "")
        foreach(LIBRARY ${LIBRARIES})
                string(CONCAT EXPORTARGS ${EXPORTARGS} ${${LIBRARY}_LIBRARY_PATH}:)
        endforeach()

        add_custom_target(
            qmake-build
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/
            COMMAND ${CMAKE_COMMAND} -E remove_directory ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/${NAME}.app
            COMMAND qmake ${QMAKEARGS} ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.pro -o ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/Makefile
            COMMAND make
            COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/${NAME}.app/Contents/Frameworks
            COMMAND macdeployqt ${NAME}.app -qmldir=${CMAKE_CURRENT_SOURCE_DIR}/src
        )
    else ()
        add_custom_target(
            qmake-build
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/
            COMMAND qmake ${QMAKEARGS} ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.pro -o ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/Makefile
            COMMAND make
        )
    endif()
endmacro()
